﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Web;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)){
            Time.timeScale = 0;
        } else if (Input.GetKey(KeyCode.LeftControl)){
            Time.timeScale = 1;
        } else if (Input.GetKey(KeyCode.Escape)){
            SceneManager.LoadScene(0);
        }
    }
}
