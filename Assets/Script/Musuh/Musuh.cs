﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musuh : MonoBehaviour
{
    
    public Transform target;
    Rigidbody rig; 
    
    public float speed = 3;
    private float radius = 2f;

    private bool pathChecker;

    public bool isWalking = false;

    public float stoppingDistance = 1f;

    private Camera _camera;

    private Vector3[] _targetPath;

    private int _indexPath = 0;

    private Animator _animator;


    private void Start()
    {
        _animator = this.gameObject.GetComponent<Animator>();
        _camera = Camera.main;

        rig = GetComponent<Rigidbody>();
    }

    private void LateUpdate() {
        // force lock rotation
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);    
    }
    private void FixedUpdate()
    {
        // if(Input.GetKeyDown("space"))
        // {
        //     SetNewTarget();
        // }

        // if(_targetPath == null )
        // {
        //     return;
        // }
        PathRequest pathRequest = new PathRequest(transform.position, target.position, OnRequestReceived);

        PathRequestManager.Singleton.Request(pathRequest);

            // if there any path
            pathChecker = true;
            // Check collider around enemies with radius
            Collider[] area = Physics.OverlapSphere(transform.position, radius);
            // Check if there any target collider
            for (int i = 0; i < area.Length; i++)
            {
                if (area[i].name == target.name)
                {
                    // set pathChecker to false if there no path again
                    pathChecker = false;
                }
            }
            if (pathChecker == true)
            {
                MoveToTarget(isWalking = true);
                _animator.SetBool("attack", false);
                _animator.SetBool("walk", true);
            } else if (pathChecker == false)
            {
                MoveToTarget(isWalking = false);
                 _animator.SetBool("walk", false);
                 _animator.SetBool("attack", true);
            }
    }

    void MoveToTarget(bool walk)
    {   
        if (_indexPath  >=  _targetPath.Length)
        {
            return;
        }
        
        // if there no player
        if (walk)
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPath[_indexPath], speed * Time.deltaTime); 
            
            // Rather than transform the position, this will move the rigidbody
            // var post = Vector3.MoveTowards(transform.position, _targetPath[_indexPath], speed * Time.deltaTime); 
            // rig.MovePosition(post);

            // Rotate to face target
            RotateToTarget(_targetPath[_indexPath]);
        }

        float distanceToTheNextWayPoint = Vector3.Distance(transform.position, _targetPath[_indexPath]);

        float distanceToFinaltWayPoint= Vector3.Distance(transform.position, _targetPath[_targetPath.Length - 1]);

        if (distanceToTheNextWayPoint < 0.05f)
        {
            _indexPath++;
        }

        if(distanceToFinaltWayPoint < stoppingDistance)
        {
            _indexPath = _targetPath.Length;
        }
    }

    void RotateToTarget(Vector3 tar)
    {
        transform.LookAt(tar);
        
        // Same concept but rather using lookat, manually rotate object view
        // // Get target dir
        // Vector2 targetDir = target.position - transform.position;
        // // Rotate toward vector
        // Vector2 newDir = Vector3.RotateTowards(transform.forward, targetDir, (speed * Time.deltaTime), 0.0f);
        // // Just Pointer
        // Debug.DrawRay(transform.position, newDir, Color.red);
        // // Calculate rotation
        // transform.rotation = Quaternion.LookRotation(newDir);
    }


    void SetNewTarget()
    {

        // Vector3 Pos = Input.mousePosition;
        // Pos.z = 20;

        // Vector3 mouseWorldPosition = _camera.ScreenToWorldPoint(Pos);

        PathRequest pathRequest = new PathRequest(transform.position, target.position, OnRequestReceived);

        PathRequestManager.Singleton.Request(pathRequest);

    }

    public void OnRequestReceived(Vector3[] path, bool succes)
    {
        _targetPath = path;
        _indexPath = 0;
    }

    public void OnDrawGizmos()
    {
        if (_targetPath != null)
        {
            for (int i = _indexPath; i < _targetPath.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(_targetPath[i], Vector3.one);

                if(i == _indexPath)
                {
                    Gizmos.DrawLine(transform.position, _targetPath[i]);
                } else
                {
                    Gizmos.DrawLine(_targetPath[i - 1], _targetPath[i]);
                }
            }
        }
    }

}
