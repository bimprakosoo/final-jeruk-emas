﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void StartScene(){
        SceneManager.LoadScene(1); //load scene
    }

    public void SettingScene(){
        SceneManager.LoadScene(2);
    }

    public void BackScene(){
        SceneManager.LoadScene(0);
    }

    public void AboutScene(){
        SceneManager.LoadScene(3);
    }
    
    public void QuitScene(){
        Application.Quit(); //Quit scene

    }
}
